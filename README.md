# Google Code-In

Google Code-in is a contest to introduce pre-university students (ages 13-17)
to the world of open source. Students must complete tasks, one at a time. It is
sponsored and run by Google. GitMate is participating in Google Code-in 2017 as
an umbrella of coala. Check out the contest rules [here](https://developers.google.com/open-source/gci/resources/contest-rules).

To join with us during GCI, we have a few tasks for you. Learn more about it in the [Wiki!](https://gitlab.com/gitmate/open-source/GCI/wikis/home)

## Getting started

GitMate is a Continuous Integration that automates code review on your projects
hosted on GitHub and GitLab to speed up the development process. GitMate
provides static code analysis powered by [coala](https://coala.io) and many more
useful plugins specially tailored for your need.

Can't find a plugin you need? [Report an issue](https://gitlab.com/gitmate/open-source/gitmate-2/issues)
or [create your own!](https://docs.gitmate.io/Developers/Writing_Plugin)

If you are new to git, check out our [guide](http://docs.coala.io/en/latest/Developers/Git_Basics.html)
which introduces you to basic git commands that would be helpful while working
with us. Check out our [documentation](http://docs.gitmate.io) for more info.

## Ask for help

You can contact us through our [slack channel](https://join.slack.com/t/gitmate/shared_invite/enQtMjYwNDM3NTkxMTM3LWZiMjEwODgwYmFjYTE5ZGJlNmJjNGZiNDdlNjA3YzI2YzEyZmIzZGU1NmFiNWI2OTkxMDI5ZWY4YTk3ZDJmYzU).

## Mentors

- [sils](https://gitlab.com/sils)
- [nkprince007](https://gitlab.com/nkprince007)
- [fneu](https://gitlab.com/fneu)
- [yuki_is_bored](https://gitlab.com/yuki_is_bored)
